use std::collections::BTreeSet;
use std::fs;

fn main() {
    let mut input = fs::read_to_string("examples/day10/input.txt")
        .expect("input file error")
        .split_terminator("\n")
        .map(|x| x.parse::<u8>().expect("cannot parse to u8"))
        .collect::<BTreeSet<_>>();

    // insert the charging outlet with a joltage of 0
    input.insert(0);

    // also insert the built-in adapter with a joltage of the top rated one + 3
    let last = input.iter().next_back().unwrap() + 3;
    input.insert(last);

    let input = input.into_iter().collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[u8]) {
    let mut j1 = 0;
    let mut j3 = 0;

    for i in 0..input.len() - 1 {
        match input[i + 1] - input[i] {
            1 => j1 += 1,
            3 => j3 += 1,
            _ => (),
        }
    }

    println!(
        "one jolt differences {} - three jolt differences {} - multiplied {}",
        j1,
        j3,
        j1 * j3
    );
}

fn part2(input: &[u8]) {
    let mut vec = vec![0 as u64; input.len()];
    vec[0] = 1;

    for i in 0..input.len() - 1 {
        let max = match input.len() - (i + 1) {
            m if m < 4 => m + 1,
            _ => 4,
        };

        for j in 1..max {
            if input[i + j] - input[i] <= 3 {
                vec[i + j] += vec[i];
            }
        }
    }

    match vec.last() {
        Some(x) => println!("distinct ways {}", x),
        None => println!("we have a problem")
    }
}
