use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day5/input.txt")
        .expect("input file error")
        .split_terminator("\n")
        .map(|x| BoardingPass::parse(&x))
        .collect::<Vec<_>>();
    
    part1(&input);
    part2(&input);
}

fn part1(input: &[BoardingPass]) {
    let mut highest = &input[0];
    for boarding_pass in input[1..].iter() {
        if boarding_pass.get_seat_id() > highest.get_seat_id() {
            highest = boarding_pass;
        }
    }
    println!("highest boarding seat id {}", highest.get_seat_id());
}

fn part2(input: &[BoardingPass]) {
    let mut ids = input.iter().map(|x| x.get_seat_id()).collect::<Vec<_>>();
    ids.sort();

    for (i, &id) in ids[..ids.len() - 1].iter().enumerate() {
        if id == ids[i + 1] - 2 {
            println!("found seat id {}", id + 1);
            break;
        }
    }
}

struct BoardingPass {
    seat: String,
}

impl BoardingPass {
    fn parse(input: &str) -> BoardingPass {
        BoardingPass {
            seat: input.to_string(),
        }
    }

    fn get_seat_id(&self) -> u16 {
        let row = self.get_row();
        let col = self.get_column();

        row as u16 * 8 + col as u16
    }

    fn get_row(&self) -> u8 {
        let mut row = 0;

        for c in self.seat[0..7].chars() {
            // shift 1 left and then add 1 or 0
            row = row << 1 | if c == 'B' { 1 } else { 0 };
        }

        row
    }

    fn get_column(&self) -> u8 {
        let mut col = 0;

        for c in self.seat[7..10].chars() {
            // shift 1 left and then add 1 or 0
           col = col << 1 | if c == 'R' { 1 } else { 0 };
        }

        col
    }
}
