use std::collections::HashSet;
use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day8/input.txt")
        .expect("input file error")
        .split_terminator("\n")
        .map(|x| Instruction::parse(&x))
        .collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[Instruction]) {
    let mut processor = Processor {
        idx: 0,
        acc: 0,
    };

    processor.is_inf(&input, &Option::None);
    println!("acc {}", processor.acc);
}

fn part2(input: &[Instruction]) {
    let mut processor = Processor {
        idx: 0,
        acc: 0,
    };

    let mut repl = Option::None;
    let mut idx = 0;

    // brute force until we find the correct instruction to change...
    while processor.is_inf(input, &repl) {
        loop {
            match &input[idx] {
                Instruction::Acc(_) => idx += 1,
                Instruction::Jmp(x) => {
                    repl = Option::Some(Replacement {
                        idx: idx as i32,
                        instr: Instruction::Nop(*x),
                    });
                    break
                }
                Instruction::Nop(x) => {
                    repl = Option::Some(Replacement {
                        idx: idx as i32,
                        instr: Instruction::Jmp(*x),
                    });
                    break
                }
            }
        }

        idx += 1;
        processor.reset();
    }

    println!("acc {}", processor.acc);
}

struct Replacement {
    idx: i32,
    instr: Instruction,
}

struct Processor {
    idx: i32,
    acc: i32,
}

impl Processor {
    fn is_inf(&mut self, instructions: &[Instruction], replacement: &Option<Replacement>) -> bool {
        let mut set = HashSet::new();

        loop {
            // if we've reached the last instruction, we can assume there is no infinite loop
            if self.idx as usize == instructions.len() {
                return false;
            }

            // if an instruction is run more than once we've detected an infinite loop
            if set.contains(&self.idx) {
                return true;
            } else {
                set.insert(self.idx);
            }

            // decide if we want to use a replacement instruction for the current index
            let instr = match replacement {
                Some(x) if self.idx == x.idx => &x.instr,
                _ => &instructions[self.idx as usize],
            };

            match instr {
                Instruction::Nop(_) => self.idx += 1,
                Instruction::Acc(x) => {
                    self.acc += *x as i32;
                    self.idx += 1;
                }
                Instruction::Jmp(x) => self.idx += *x as i32,
            }
        }
    }

    fn reset(&mut self) {
        self.idx = 0;
        self.acc = 0;
    }
}

enum Instruction {
    Nop(i16),
    Acc(i16),
    Jmp(i16),
}

impl Instruction {
    fn parse(input: &str) -> Instruction {
        match &input[0..3] {
            "nop" => Instruction::Nop(
                input[4..]
                    .to_string()
                    .parse::<i16>()
                    .expect("nop not a number"),
            ),
            "acc" => Instruction::Acc(
                input[4..]
                    .to_string()
                    .parse::<i16>()
                    .expect("acc not a number"),
            ),
            "jmp" => Instruction::Jmp(
                input[4..]
                    .to_string()
                    .parse::<i16>()
                    .expect("jmp not a number"),
            ),
            _ => panic!("unknown input {}", input[4..].to_string()),
        }
    }
}
