use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day1/input.txt")
        .expect("input file error")
        .lines()
        .map(|x| x.parse::<i32>().unwrap())
        .collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[i32]) {
    for i in 0..input.len() {
        for j in i + 1..input.len() {
            if input[i] + input[j] == 2020 {
                println!(
                    "found {}, {} - multiplied {}",
                    input[i],
                    input[j],
                    input[i] * input[j]
                );
                return;
            }
        }
    }
}

fn part2(input: &[i32]) {
    for i in 0..input.len() {
        for j in i + 1..input.len() {
            // no need to continue if these two are equal to or more than 2020
            if input[i] + input[j] >= 2020 {
                continue;
            }

            for k in j + 1..input.len() {
                if input[i] + input[j] + input[k] == 2020 {
                    println!(
                        "found {}, {}, {} - multiplied {}",
                        input[i],
                        input[j],
                        input[k],
                        input[i] * input[j] * input[k]
                    );
                    return;
                }
            }
        }
    }
}
