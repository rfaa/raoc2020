use std::collections::HashMap;
use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day7/input.txt")
        .expect("input file error")
        .split_terminator("\n")
        .fold(HashMap::new(), |mut map, x| {
            let bag = Bag::parse(x);
            map.insert(bag.color, bag.rules);
            map
        });

    part1(&input);
    part2(&input);
}

fn part1(input: &HashMap<String, Vec<Rule>>) {
    let mut total = 0;
    let target_color = "shiny gold";

    for (color, rules) in input.iter() {
        if color == target_color {
            continue;
        }

        for rule in rules {
            if contains(input, target_color, &rule.bag_color) {
                total += 1;
                break;
            }
        }
    }

    println!(
        "{} bag can ultimately be contained by {} bags",
        target_color, total
    );
}

fn part2(input: &HashMap<String, Vec<Rule>>) {
    let target_color = "shiny gold";
    let total = total(input, target_color);

    println!("{} contains total {} bags", target_color, total);
}

fn total(rule_book: &HashMap<String, Vec<Rule>>, target_color: &str) -> u32 {
    let rules = rule_book.get(target_color).expect("rules not found");
    let mut amount = 0;

    for rule in rules {
        // add the amount of bags times whatever they have in turn
        amount += rule.amount as u32 * total(rule_book, &rule.bag_color);

        // also add the current bags
        amount += rule.amount as u32;
    }

    amount
}

fn contains(rule_book: &HashMap<String, Vec<Rule>>, target_color: &str, from_color: &str) -> bool {
    let rules = rule_book.get(from_color).expect("rules not found");

    if from_color == target_color {
        return true;
    }

    for rule in rules {
        if rule.bag_color == target_color {
            return true;
        }

        if contains(rule_book, target_color, &rule.bag_color) {
            return true;
        }
    }

    false
}

struct Bag {
    color: String,
    rules: Vec<Rule>,
}

struct Rule {
    amount: u8,
    bag_color: String,
}

impl Bag {
    fn parse(input: &str) -> Bag {
        let split = input.split_whitespace().collect::<Vec<_>>();
        let color = split[0..2].join(" ");
        let mut rules = Vec::new();

        for i in (4..split.len()).step_by(4) {
            if split[i] == "no" {
                break;
            }

            rules.push(Rule {
                amount: split[i].to_string().parse::<u8>().expect("not a number"),
                bag_color: split[i + 1..i + 3].join(" "),
            });
        }

        Bag {
            color: color,
            rules: rules,
        }
    }
}
