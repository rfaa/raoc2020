use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day11/input.txt")
        .expect("input file error")
        .lines()
        .map(|x| x.to_string())
        .collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    let mut map = input
        .iter()
        .map(|x| Tile::parse(&x))
        .collect::<Vec<Vec<_>>>();
    let mut mutations = Vec::with_capacity(map.len());

    // helper function to get min and max values for x and y
    fn get_min_max(x: usize, x_max: usize, y: usize, y_max: usize) -> (usize, usize, usize, usize) {
        (
            if x > 0 && x < x_max { x - 1 } else { x }, // x min
            if x + 1 >= x_max { x } else { x + 1 },     // x max
            if y > 0 { y - 1 } else { 0 },              // y min
            if y <= y_max - 2 { y + 1 } else { y },     // y max
        )
    }

    loop {
        mutations.clear();

        for y in 0..map.len() {
            for x in 0..map[y].len() {
                match map[y][x] {
                    Tile::Floor => {}
                    Tile::Empty => {
                        // decide if we should occupy this empty seat
                        // rule: all adjacent seats must not be occupied to occupy

                        let (x_min, x_max, y_min, y_max) =
                            get_min_max(x, map[y].len(), y, map.len());

                        let mut found_occupied = false;
                        for i in y_min..=y_max {
                            for j in x_min..=x_max {
                                // same position, try next
                                if i == y && j == x {
                                    continue;
                                }

                                // if there's an occupied seat nearby we can exit early,
                                // seat should not change
                                if map[i][j] == Tile::Occupied {
                                    found_occupied = true;
                                    break;
                                }
                            }

                            // occupied found, exit early
                            if found_occupied {
                                break;
                            }
                        }

                        // no occupied seats found, let's occupy
                        if !found_occupied {
                            mutations.push((x, y, Tile::Occupied));
                        }
                    }
                    Tile::Occupied => {
                        // decide if we should empty this occupied seat
                        // rule: at least 4 adjacent seats must be occupied to empty

                        let (x_min, x_max, y_min, y_max) =
                            get_min_max(x, map[y].len(), y, map.len());

                        let mut occupied = 0;
                        for i in y_min..=y_max {
                            for j in x_min..=x_max {
                                // same position, try next
                                if i == y && j == x {
                                    continue;
                                }

                                // increment if there's an occupied seat nearby
                                if map[i][j] == Tile::Occupied {
                                    occupied += 1;

                                    // at least 4 adjacent seats were occupied, let's empty
                                    if occupied >= 4 {
                                        mutations.push((x, y, Tile::Empty));
                                        break;
                                    }
                                }
                            }

                            // at least 4 occupied adjacent seats found, exit early
                            if occupied >= 4 {
                                break;
                            }
                        }
                    }
                }
            }
        }

        // no new mutations, map has stabilized
        if mutations.len() == 0 {
            break;
        }

        // add the mutations to the map
        for (x, y, tile) in mutations.iter() {
            map[*y][*x] = *tile;
        }
    }

    // count total number of occupied seats
    let occupied = map.iter().fold(0, |a, y| {
        a + y.iter().fold(0, |b, x| match x {
            Tile::Occupied => b + 1,
            _ => b,
        })
    });

    println!("found {} occupied seats", occupied);
}

fn part2(input: &[String]) {
    let mut map = input
        .iter()
        .map(|x| Tile::parse(&x))
        .collect::<Vec<Vec<_>>>();
    let directions = directions::get_all_directions();
    let mut mutations = Vec::with_capacity(map.len());

    loop {
        mutations.clear();

        for y in 0..map.len() {
            for x in 0..map[y].len() {
                match map[y][x] {
                    Tile::Floor => {}
                    Tile::Empty => {
                        // decide if we should occupy this empty seat
                        // rule: all visible seats in all directions must not be occupied to occupy

                        let mut pos = Position { x, y };
                        let mut ix = 0;

                        // check all the directions
                        while ix != directions.len() {
                            match get_next_pos(&pos, map[y].len(), map.len(), directions[ix]) {
                                Some(next_pos) => {
                                    match map[next_pos.y][next_pos.x] {
                                        // if floor we need to check next pos in same direction
                                        Tile::Floor => pos = next_pos,
                                        // if empty we can continue with next direction
                                        Tile::Empty => {
                                            ix += 1;
                                            pos = Position { x, y };
                                        }
                                        // if occupied we cannot occupy, exit early
                                        Tile::Occupied => break,
                                    }
                                }
                                // reached the end of map, continue with next direction
                                None => {
                                    ix += 1;
                                    pos = Position { x, y };
                                }
                            }
                        }

                        // no more directions to check, let's occupy
                        if ix == directions.len() {
                            mutations.push((x, y, Tile::Occupied));
                        }
                    }
                    Tile::Occupied => {
                        // decide if we should empty this occupied seat
                        // rule: at least 5 visible seats in all directions must be occupied to empty

                        let mut pos = Position { x, y };
                        let mut occupied = 0;
                        let mut ix = 0;

                        // check all the directions
                        while ix != directions.len() {
                            match get_next_pos(&pos, map[y].len(), map.len(), directions[ix]) {
                                Some(next_pos) => {
                                    match map[next_pos.y][next_pos.x] {
                                        // if floor we need to check next pos in same direction
                                        Tile::Floor => pos = next_pos,
                                        // if empty we can continue with next direction
                                        Tile::Empty => {
                                            ix += 1;
                                            pos = Position { x, y };
                                        }
                                        // we need to find 5 occupied seats
                                        Tile::Occupied => {
                                            occupied += 1;
                                            ix += 1;
                                            pos = Position { x, y };

                                            // 5 occupied seats found, let's empty
                                            if occupied >= 5 {
                                                mutations.push((x, y, Tile::Empty));
                                                break;
                                            }
                                        }
                                    }
                                }
                                // reached the end of map, continue with next direction
                                None => {
                                    ix += 1;
                                    pos = Position { x, y };
                                }
                            }
                        }
                    }
                }
            }
        }

        // no new mutations, map has stabilized
        if mutations.len() == 0 {
            break;
        }

        // add mutations to the map
        for (x, y, tile) in mutations.iter() {
            map[*y][*x] = *tile;
        }

        mutations.clear();
    }

    // count total number of occupied seats
    let occupied = map.iter().fold(0, |a, y| {
        a + y.iter().fold(0, |b, x| match x {
            Tile::Occupied => b + 1,
            _ => b,
        })
    });

    println!("found {} occupied seats", occupied);
}

struct Position {
    x: usize,
    y: usize,
}

mod directions {
    pub const NORTH: u8 = 0b1000;
    pub const WEST: u8 = 0b0100;
    pub const SOUTH: u8 = 0b0010;
    pub const EAST: u8 = 0b0001;

    pub const fn get_all_directions() -> [u8; 8] {
        [
            NORTH,
            NORTH | EAST,
            EAST,
            SOUTH | EAST,
            SOUTH,
            SOUTH | WEST,
            WEST,
            NORTH | WEST,
        ]
    }
}

#[derive(Copy, Clone, PartialEq)]
enum Tile {
    Floor,
    Empty,
    Occupied,
}

impl Tile {
    fn parse(row: &str) -> Vec<Tile> {
        row.as_bytes()
            .iter()
            .map(|x| match x {
                b'.' => Tile::Floor,
                b'L' => Tile::Empty,
                b'#' => Tile::Occupied,
                _ => panic!("unknwon tile {}", x),
            })
            .collect::<Vec<_>>()
    }
}

fn get_next_pos(current: &Position, x_max: usize, y_max: usize, direction: u8) -> Option<Position> {
    let mut pos = Position {
        x: current.x,
        y: current.y,
    };

    // north
    if direction & directions::NORTH != 0 {
        if current.y > 0 {
            pos.y -= 1;
        } else {
            return Option::None;
        }
    }

    // south
    if direction & directions::SOUTH != 0 {
        if current.y + 1 < y_max {
            pos.y += 1;
        } else {
            return Option::None;
        }
    }

    // west
    if direction & directions::WEST != 0 {
        if current.x > 0 {
            pos.x -= 1;
        } else {
            return Option::None;
        }
    }

    // east
    if direction & directions::EAST != 0 {
        if current.x + 1 < x_max {
            pos.x += 1;
        } else {
            return Option::None;
        }
    }

    Option::Some(pos)
}
