use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day2/input.txt")
        .expect("input file error")
        .lines()
        .map(|x| Policy::parse(&x))
        .collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[Policy]) {
    let num = input.iter().filter(|x| x.is_valid_v1()).count();
    println!("valid one {}", num);
}

fn part2(input: &[Policy]) {
    let num = input.iter().filter(|x| x.is_valid_v2()).count();
    println!("valid two {}", num);
}

struct Policy {
    min: usize,
    max: usize,
    letter: char,
    password: String,
}

impl Policy {
    fn is_valid_v1(&self) -> bool {
        let count = self.password.matches(self.letter).count();

        count >= self.min && count <= self.max
    }

    fn is_valid_v2(&self) -> bool {
        let vec = self.password.chars().collect::<Vec<_>>();
        let one = vec[self.min - 1] == self.letter;
        let two = vec[self.max - 1] == self.letter;

        one ^ two
    }

    fn parse(input: &str) -> Policy {
        let vec = input.split(&[' ', '-'][..]).collect::<Vec<_>>();

        Policy {
            min: vec[0].parse::<usize>().unwrap(),
            max: vec[1].parse::<usize>().unwrap(),
            letter: vec[2].chars().nth(0).unwrap(),
            password: vec[3].to_string(),
        }
    }
}
