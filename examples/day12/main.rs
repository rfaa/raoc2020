use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day12/input.txt")
        .expect("input file error")
        .lines()
        .map(|x| Action::parse(x))
        .collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[Action]) {
    let mut horizontal = 0; // east positive - west negative
    let mut vertical = 0; // north positive - south negative
    let mut degrees = 90; // north 0, east 90, south 180, west 270

    // modulo implementation since rust % is the remainder and not the modulo operator
    let m = |a: i16, b: i16| ((a % b) + b) % b;

    for action in input {
        match action {
            Action::North(x) => vertical += x,
            Action::South(x) => vertical -= x,
            Action::East(x) => horizontal += x,
            Action::West(x) => horizontal -= x,
            Action::Left(x) => {
                degrees -= x;
                degrees = m(degrees, 360);
            }
            Action::Right(x) => {
                degrees += x;
                degrees = m(degrees, 360);
            }
            Action::Forward(x) => {
                match degrees {
                    0 => vertical += x,     // north
                    90 => horizontal += x,  // east
                    180 => vertical -= x,   // south
                    270 => horizontal -= x, // west
                    _ => panic!("invalid degrees {}", degrees),
                }
            }
        }
    }

    println!(
        "{} {} {} - manhattan {}",
        horizontal,
        vertical,
        degrees,
        horizontal.abs() + vertical.abs()
    );
}

fn part2(input: &[Action]) {
    let mut waypoint_horizontal = 10; // east positive - west negative
    let mut waypoint_vertical = 1; // north positive - south negative
    let mut ship_horizontal = 0;
    let mut ship_vertical = 0;

    // helper closure to turn 90 degrees, left or right
    // turning 90 degrees left is the same as turning 270 degrees right and vice versa
    let turn90 = |v: &mut i16, h: &mut i16, left| {
        let tmp_h = *v * if left { -1 } else { 1 };
        let tmp_v = *h * if left { 1 } else { -1 };
        *v = tmp_v;
        *h = tmp_h;
    };

    for action in input {
        match action {
            Action::North(x) => waypoint_vertical += x,
            Action::South(x) => waypoint_vertical -= x,
            Action::East(x) => waypoint_horizontal += x,
            Action::West(x) => waypoint_horizontal -= x,
            Action::Left(x) | Action::Right(x) if *x == 180 => {
                waypoint_vertical *= -1;
                waypoint_horizontal *= -1;
            }
            Action::Left(x) => match x {
                90 => turn90(&mut waypoint_vertical, &mut waypoint_horizontal, true),
                270 => turn90(&mut waypoint_vertical, &mut waypoint_horizontal, false),
                _ => panic!("invalid degrees left {}", x),
            },
            Action::Right(x) => match x {
                90 => turn90(&mut waypoint_vertical, &mut waypoint_horizontal, false),
                270 => turn90(&mut waypoint_vertical, &mut waypoint_horizontal, true),
                _ => panic!("invalid degrees right {}", x),
            },
            Action::Forward(x) => {
                ship_horizontal += waypoint_horizontal * *x;
                ship_vertical += waypoint_vertical * *x;
            }
        }
    }

    println!(
        "wh {} wv {} sv {} sh {} - manhattan {}",
        waypoint_horizontal,
        waypoint_vertical,
        ship_horizontal,
        ship_vertical,
        ship_horizontal.abs() + ship_vertical.abs()
    );
}

enum Action {
    North(i16),
    South(i16),
    East(i16),
    West(i16),
    Left(i16),
    Right(i16),
    Forward(i16),
}

impl Action {
    fn parse(input: &str) -> Action {
        let p = |s: &str| {
            (
                s[0..1].parse::<char>().expect("not char"),
                s[1..].parse::<i16>().expect("not i16"),
            )
        };

        let (a, v) = p(input);

        match a {
            'N' => Action::North(v),
            'S' => Action::South(v),
            'E' => Action::East(v),
            'W' => Action::West(v),
            'L' => Action::Left(v),
            'R' => Action::Right(v),
            'F' => Action::Forward(v),
            _ => panic!("unknown input {} {}", a, v),
        }
    }
}
