use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day3/input.txt")
        .expect("input file error")
        .lines()
        .map(|x| x.chars().collect())
        .collect::<Vec<Vec<_>>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &Vec<Vec<char>>) {
    let trees_found = traverse(input, 3, 1);
    println!("found {} trees", trees_found);
}

fn part2(input: &Vec<Vec<char>>) {
    let slopes: [Slope; 5] = [
        Slope { right: 1, down: 1 },
        Slope { right: 3, down: 1 },
        Slope { right: 5, down: 1 },
        Slope { right: 7, down: 1 },
        Slope { right: 1, down: 2 },
    ];

    let mut slope_trees: [i32; 5] = [0; 5];
    let mut multiplied_trees: i64 = 1;

    for (i, slope) in slopes.iter().enumerate() {
        slope_trees[i] = traverse(input, slope.right, slope.down);

        multiplied_trees *= i64::from(slope_trees[i]);
        println!("slope {} found {} trees", i, slope_trees[i]);
    }

    println!("trees found in slopes multiplied {}", multiplied_trees);
}

/// Traverses the map and returns the total number of trees you encounter after each movement.
fn traverse(input: &Vec<Vec<char>>, x_speed: usize, y_speed: usize) -> i32 {
    // we know the size of x stays constant regardless of row,
    // so let's just check the first one and be done with it
    let x_max = input[0].len();
    let y_max = input.len() - 1;

    let mut pos = Position { x: 0, y: 0 };
    let mut trees_found = 0;

    loop {
        pos.x = (pos.x + x_speed) % x_max;
        pos.y += y_speed;

        if input[pos.y][pos.x] == '#' {
            trees_found += 1;
        }

        if pos.y >= y_max {
            break;
        }
    }

    trees_found
}

struct Position {
    x: usize,
    y: usize,
}

struct Slope {
    right: usize,
    down: usize,
}
