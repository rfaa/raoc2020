use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day9/input.txt")
        .expect("input file error")
        .split_terminator("\n")
        .map(|x| x.parse::<u64>().expect("cannot parse to u64"))
        .collect::<Vec<_>>();

    println!("{}", part1(&input, 25));
    part2(&input);
}

fn part1(input: &[u64], preamble: usize) -> u64 {
    for i in preamble..input.len() {
        let target = input[i];
        let mut found = false;

        for j in i - preamble..i - 1 {
            for k in j + 1..i {
                if input[j] + input[k] == target && input[j] != input[k] {
                    found = true;
                    break;
                }
            }

            if found {
                break;
            }
        }

        if !found {
            return target;
        }
    }

    0
}

fn part2(input: &[u64]) {
    let target = part1(&input, 25);

    for i in 0..input.len() {
        let mut sum = input[i];
        for j in i + 1..input.len() - 1 {
            sum += input[j];

            if sum > target {
                break;
            }

            if sum == target {
                let mut smallest = input[i];
                let mut largest = input[i];

                for k in i..j {
                    if smallest > input[k] {
                        smallest = input[k];
                    }

                    if largest < input[k] {
                        largest = input[k];
                    }
                }

                println!(
                    "found smallest {} and largest {} which together is {}",
                    smallest,
                    largest,
                    smallest + largest
                );
            }
        }
    }
}
