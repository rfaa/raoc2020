use std::collections::{HashSet,HashMap};
use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day6/input.txt")
        .expect("input file error")
        .split_terminator("\n\n")
        .map(|x| GroupResult::parse(&x))
        .collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[GroupResult]) {
    let total = input.iter().fold(0, |sum, i| sum + i.get_anyone_count());
    println!("anyone {}", total);
}

fn part2(input: &[GroupResult]) {
    let total = input.iter().fold(0, |sum, i| sum + i.get_all_count());
    println!("all {}", total);
}

struct GroupResult {
    answers: Vec<String>,
}

impl GroupResult {
    fn parse(input: &str) -> GroupResult {
        GroupResult {
            answers: input.lines().map(|x| x.to_string()).collect(),
        }
    }

    fn get_anyone_count(&self) -> usize {
        let mut set = HashSet::new();

        for w in self.answers.iter() {
            for c in w.chars() {
                set.insert(c);
            }
        }

        set.len()
    }

    fn get_all_count(&self) -> usize {
        let mut map = HashMap::new();

        for w in self.answers.iter() {
            for c in w.chars() {
                let count = map.entry(c).or_insert(0);
                *count += 1;
            }
        }

        map.iter().filter(|&(_, &v)| v == self.answers.len()).count()
    }
}
