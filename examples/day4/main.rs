use std::fs;

fn main() {
    let input = fs::read_to_string("examples/day4/input.txt")
        .expect("input file error")
        .split("\n\n")
        .map(|x| Passport::parse(&x))
        .collect::<Vec<_>>();

    part1(&input);
    part2(&input);
}

fn part1(input: &[Passport]) {
    let valid_passports = input.iter().filter(|x| x.is_valid_v1()).count();
    println!("valid passports v1 {}", valid_passports);
}

fn part2(input: &[Passport]) {
    let valid_passports = input.iter().filter(|x| x.is_valid_v2()).count();
    println!("valid passports v2 {}", valid_passports);
}

struct Passport {
    birth_year: Option<String>,
    issue_year: Option<String>,
    expiration_year: Option<String>,
    height: Option<String>,
    hair_color: Option<String>,
    eye_color: Option<String>,
    passport_id: Option<String>,
    country_id: Option<String>,
}

impl Passport {
    fn is_valid_v1(&self) -> bool {
        self.birth_year.is_some()
            && self.issue_year.is_some()
            && self.expiration_year.is_some()
            && self.height.is_some()
            && self.hair_color.is_some()
            && self.eye_color.is_some()
            && self.passport_id.is_some()
    }

    fn is_valid_v2(&self) -> bool {
        let valid_birth_year = match &self.birth_year {
            Some(x) => match x.parse::<u16>() {
                Ok(x) => (x >= 1920 && x <= 2002),
                Err(_) => false,
            },
            None => false,
        };

        let valid_issue_year = match &self.issue_year {
            Some(x) => match x.parse::<u16>() {
                Ok(x) => (x >= 2010 && x <= 2020),
                Err(_) => false,
            },
            None => false,
        };

        let valid_expiration_year = match &self.expiration_year {
            Some(x) => match x.parse::<u16>() {
                Ok(x) => (x >= 2020 && x <= 2030),
                Err(_) => false,
            },
            None => false,
        };

        let valid_height = match &self.height {
            Some(x) if x.len() >= 3 => {
                // we know this is ascii only so this is fine
                let unit = &x[x.len() - 2..];
                let value = &x[0..x.len() - 2];
                match value.parse::<u8>() {
                    Ok(y) => match unit {
                        "cm" => (y >= 150 && y <= 193),
                        "in" => (y >= 59 && y <= 76),
                        _ => false,
                    },
                    Err(_) => false,
                }
            }
            _ => false,
        };

        let valid_hair_color = match &self.hair_color {
            Some(x) => {
                // could've used a regex for all this but let's only use the standard lib
                if x.len() != 7 {
                    return false;
                }

                // a valid hex color starts with hash
                if !x.starts_with('#') {
                    return false;
                }

                // we know it's all ascii so let's get the byte values
                // 0 = 48, 9 = 57, a = 97, f = 102
                for &b in x[1..].as_bytes() {
                    if (b < 48 || b > 57) && (b < 97 || b > 102) {
                        return false;
                    }
                }

                true
            }
            None => false,
        };

        let valid_eye_color = match &self.eye_color {
            Some(x) => {
                let valid_eye_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
                valid_eye_colors.iter().any(|y| y == x)
            }
            None => false,
        };

        let valid_passport_id = match &self.passport_id {
            Some(x) => x.len() == 9 && x.chars().all(char::is_numeric),
            None => false,
        };

        valid_birth_year
            && valid_issue_year
            && valid_expiration_year
            && valid_height
            && valid_hair_color
            && valid_eye_color
            && valid_passport_id
    }

    fn parse(input: &str) -> Passport {
        let fields = input.split(&[' ', '\n'][..]).collect::<Vec<_>>();
        let mut passport = Passport {
            birth_year: None,
            issue_year: None,
            expiration_year: None,
            height: None,
            hair_color: None,
            eye_color: None,
            passport_id: None,
            country_id: None,
        };

        for field in fields {
            // we know the key values are specified as 'key:value'
            let kv = field.split(':').collect::<Vec<_>>();
            match kv[0] {
                "byr" => passport.birth_year = Some(kv[1].to_string()),
                "iyr" => passport.issue_year = Some(kv[1].to_string()),
                "eyr" => passport.expiration_year = Some(kv[1].to_string()),
                "hgt" => passport.height = Some(kv[1].to_string()),
                "hcl" => passport.hair_color = Some(kv[1].to_string()),
                "ecl" => passport.eye_color = Some(kv[1].to_string()),
                "pid" => passport.passport_id = Some(kv[1].to_string()),
                "cid" => passport.country_id = Some(kv[1].to_string()),
                _ => (),
            }
        }

        passport
    }
}
